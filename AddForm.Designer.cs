﻿
namespace Kino_tab
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddForm));
            this.label_Name = new System.Windows.Forms.Label();
            this.AddName = new System.Windows.Forms.TextBox();
            this.ADDDirector = new System.Windows.Forms.TextBox();
            this.label_Director = new System.Windows.Forms.Label();
            this.ADDList_of_actors = new System.Windows.Forms.TextBox();
            this.label_List_of_actors = new System.Windows.Forms.Label();
            this.ADDСontent = new System.Windows.Forms.TextBox();
            this.label_Сontent = new System.Windows.Forms.Label();
            this.btn_add = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_Name
            // 
            this.label_Name.AutoSize = true;
            this.label_Name.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Name.Location = new System.Drawing.Point(30, 37);
            this.label_Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(69, 17);
            this.label_Name.TabIndex = 1;
            this.label_Name.Text = "Название";
            // 
            // AddName
            // 
            this.AddName.Location = new System.Drawing.Point(103, 37);
            this.AddName.Margin = new System.Windows.Forms.Padding(2);
            this.AddName.Name = "AddName";
            this.AddName.Size = new System.Drawing.Size(255, 20);
            this.AddName.TabIndex = 2;
            // 
            // ADDDirector
            // 
            this.ADDDirector.Location = new System.Drawing.Point(103, 67);
            this.ADDDirector.Margin = new System.Windows.Forms.Padding(2);
            this.ADDDirector.Name = "ADDDirector";
            this.ADDDirector.Size = new System.Drawing.Size(255, 20);
            this.ADDDirector.TabIndex = 4;
            // 
            // label_Director
            // 
            this.label_Director.AutoSize = true;
            this.label_Director.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Director.Location = new System.Drawing.Point(30, 67);
            this.label_Director.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Director.Name = "label_Director";
            this.label_Director.Size = new System.Drawing.Size(69, 17);
            this.label_Director.TabIndex = 3;
            this.label_Director.Text = "Режиссёр";
            // 
            // ADDList_of_actors
            // 
            this.ADDList_of_actors.Location = new System.Drawing.Point(103, 96);
            this.ADDList_of_actors.Margin = new System.Windows.Forms.Padding(2);
            this.ADDList_of_actors.Multiline = true;
            this.ADDList_of_actors.Name = "ADDList_of_actors";
            this.ADDList_of_actors.Size = new System.Drawing.Size(255, 51);
            this.ADDList_of_actors.TabIndex = 6;
            // 
            // label_List_of_actors
            // 
            this.label_List_of_actors.AutoSize = true;
            this.label_List_of_actors.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_List_of_actors.Location = new System.Drawing.Point(43, 96);
            this.label_List_of_actors.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_List_of_actors.Name = "label_List_of_actors";
            this.label_List_of_actors.Size = new System.Drawing.Size(56, 17);
            this.label_List_of_actors.TabIndex = 5;
            this.label_List_of_actors.Text = "Актёры";
            // 
            // ADDСontent
            // 
            this.ADDСontent.Location = new System.Drawing.Point(103, 151);
            this.ADDСontent.Margin = new System.Windows.Forms.Padding(2);
            this.ADDСontent.Multiline = true;
            this.ADDСontent.Name = "ADDСontent";
            this.ADDСontent.Size = new System.Drawing.Size(255, 77);
            this.ADDСontent.TabIndex = 8;
            // 
            // label_Сontent
            // 
            this.label_Сontent.AutoSize = true;
            this.label_Сontent.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Сontent.Location = new System.Drawing.Point(11, 151);
            this.label_Сontent.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Сontent.Name = "label_Сontent";
            this.label_Сontent.Size = new System.Drawing.Size(88, 17);
            this.label_Сontent.TabIndex = 7;
            this.label_Сontent.Text = "Содержание";
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_add.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_add.Location = new System.Drawing.Point(146, 244);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(127, 54);
            this.btn_add.TabIndex = 9;
            this.btn_add.Text = "ADD";
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(410, 334);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.ADDСontent);
            this.Controls.Add(this.label_Сontent);
            this.Controls.Add(this.ADDList_of_actors);
            this.Controls.Add(this.label_List_of_actors);
            this.Controls.Add(this.ADDDirector);
            this.Controls.Add(this.label_Director);
            this.Controls.Add(this.AddName);
            this.Controls.Add(this.label_Name);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Новый фильм";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.TextBox AddName;
        private System.Windows.Forms.TextBox ADDDirector;
        private System.Windows.Forms.Label label_Director;
        private System.Windows.Forms.TextBox ADDList_of_actors;
        private System.Windows.Forms.Label label_List_of_actors;
        private System.Windows.Forms.TextBox ADDСontent;
        private System.Windows.Forms.Label label_Сontent;
        private System.Windows.Forms.Button btn_add;
    }
}