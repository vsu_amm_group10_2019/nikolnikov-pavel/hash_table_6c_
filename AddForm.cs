﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino_tab
{
    public partial class AddForm : Form
    {
        public Film Film { get; } = new Film();
        private FormState FormState;
        public AddForm(FormState formState, Film film = null)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            FormState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        this.btn_add.Text = "ADD";
                        return;
                    }
                case FormState.EDIT:
                    {
                        this.btn_add.Text = "EDIT";
                        AddName.ReadOnly = true;
                        AddName.Text = film.Name;
                        ADDDirector.Text = film.Director;
                        ADDList_of_actors.Text = film.List_of_actors;
                        ADDСontent.Text = film.Сontent;                        
                        Text = "Изменение";
                        break;
                    }
                case FormState.DELETE:
                   
                case FormState.SEARCH:
                    {
                        this.btn_add.Text = "SEARCH";
                        ADDDirector.ReadOnly = true;
                        ADDList_of_actors.ReadOnly = true;
                        ADDСontent.ReadOnly = true;
                        Text = "Поиск фильма";
                        break;
                    }

                case FormState.DISPLAY:
                    {
                        AddName.ReadOnly = true;
                        AddName.Text = film.Name;
                        ADDDirector.Text = film.Director;
                        ADDСontent.Text = film.Сontent;
                        ADDList_of_actors.Text = film.List_of_actors;

                        ADDDirector.ReadOnly = true;
                        ADDList_of_actors.ReadOnly = true;
                        ADDСontent.ReadOnly = true;
                        Text = "Фильм";
                        this.btn_add.Text = "OK";
                        break;
                    }
                    
            }
        }
        public bool ok;
        private void btn_add_Click(object sender, EventArgs e)
        {
            if (FormState == FormState.ADD || FormState == FormState.DELETE || FormState == FormState.SEARCH || FormState == FormState.EDIT)
            {
                if (AddName.Text != "")
                {
                    Film.Name = AddName.Text;
                    ok = true;
                    this.Close();
                }
                else
                {                  
                    MessageBox.Show(
                            "Название не указано!",
                            "Ошибка ввода!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                }

            }

            if  (FormState == FormState.ADD || FormState == FormState.EDIT )
            {
                  if (ADDDirector.Text != "" && ADDList_of_actors.Text != "" && ADDСontent.Text != "" && AddName.Text != "")
                  {
                     Film.Director = ADDDirector.Text;
                     Film.Сontent = ADDСontent.Text;
                     Film.List_of_actors = ADDList_of_actors.Text;
                     ok = true;
                     this.Close();
                  }
                  else
                  {
                    ok = false; //могут ли быть фильмы без сведений?
                    MessageBox.Show(
                              "Заполнены не все поля!",
                              "Ошибка ввода!",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
                   }    
                      
            }

              
              if (FormState == FormState.DISPLAY)
              {
                  ok = true;
                  this.Close();
              }
              
        }
    }
}
