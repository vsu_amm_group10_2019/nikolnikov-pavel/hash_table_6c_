﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kino_tab
{
    public class Film
    {
        public string Name { get; set; }
        public string Director { get; set; }
        public string List_of_actors { get; set; }
        public string Сontent { get; set; }
        
        public static string Hash(string name)
        {
            string result = "";
            foreach (char c in name)
            {
                result += c;
            }
            return result;
        }

    }
}
