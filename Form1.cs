﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kino_tab
{
    public partial class Form1 : Form
    {
        private string FileName { get; set; }
        LineHashTable LineHash { get; set; } = new LineHashTable(50);
        public Form1()
        {
            InitializeComponent();
        }
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm(FormState.ADD);
            addForm.ShowDialog();
            if (addForm.ok)
            {
                if (LineHash.Add(addForm.Film))
                {
                    Redraw();
                }
            }
            else
            {
                MessageBox.Show("Не удалось добавить запись");
            }
            
        }

        private void Redraw()
        {
            List<Film> films = LineHash.GetData();
            TableFilm.Rows.Clear();
            TableFilm.RowCount = films.Count;
            for (int i = 0; i < films.Count; i++)
            {
                TableFilm.Rows[i].Cells[0].Value = films[i].Name;
                TableFilm.Rows[i].Cells[1].Value = films[i].Director;
                TableFilm.Rows[i].Cells[2].Value = films[i].List_of_actors;
                TableFilm.Rows[i].Cells[3].Value = films[i].Сontent;
            }
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            string name = search.Film.Name;
            Film film = LineHash.Find(name);
            if (film != null)
            {
                AddForm edit = new AddForm(FormState.EDIT, film);
                edit.ShowDialog();
                LineHash.Delete(name);
                if (LineHash.Add(edit.Film))
                {
                    Redraw();
                }
                else
                {
                    MessageBox.Show("Не удалось добавить запись");
                }

            }
            else
            {
                MessageBox.Show("Нет такого фильма в списке");
            }
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            if (search.ok)
            {
                string name = search.Film.Name;
                Film film = LineHash.Find(name);
                if (film != null)
                {
                    AddForm show = new AddForm(FormState.DISPLAY, film);
                    show.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Нет такого фильма в списке");
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            search.ShowDialog();
            if (search.ok)
            {
                string name = search.Film.Name;
                LineHash.Delete(name);
                Redraw();
            }
        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                DialogResult result = MessageBox.Show(
                    "Сохранить открытый файл?",
                    "Открыт другой файл",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    SaveToFile();
                }

            }
            FileName = "";
            LineHash.Clear();
            Redraw();
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            else
            {
                saveAsToolStripMenuItem_Click(sender, e);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Json File|*.json";
            saveFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                FileName = saveFileDialog.FileName;
                SaveToFile();
            }
        }
        private void SaveToFile()
        {
            List<Film> films = LineHash.GetData();
            string result = JsonConvert.SerializeObject(films);
            File.WriteAllText(FileName, result);
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                DialogResult result = MessageBox.Show(
                    "Сохранить открытый файл?",
                    "Открыт другой файл",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1,
                    MessageBoxOptions.DefaultDesktopOnly);
                if (result == DialogResult.Yes)
                {
                    SaveToFile();
                }

            }
            openFileDialog.Filter = "Json File|*.json";
            openFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog.FileName))
            {
                FileName = openFileDialog.FileName;
                string txt = File.ReadAllText(FileName);
                List<Film> films = JsonConvert.DeserializeObject<List<Film>>(txt);
                LineHash.Clear();
                foreach (Film film in films)
                {
                    LineHash.Add(film);
                }
                Redraw();
            }
        }

        private void menuExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Метод отрытой адресации (линейное опробование)" +
                "\n Задан набор записей следующей структуры: название кинофильма, режиссер, список актеров, краткое содержание." +
                "\n По заданному названию фильма найти остальные сведения.",
                              "Условие задачи",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Information);
        }
    }
}
