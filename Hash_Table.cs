﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kino_tab
{
    interface HashTable
    {
        bool Add(Film film);
        Film Find(string Name);
        void Delete(string Name);
        List<Film> GetData();
        void Clear();
    }
}
