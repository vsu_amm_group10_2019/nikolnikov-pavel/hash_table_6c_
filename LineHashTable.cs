﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kino_tab
{
    class LineHashTable : HashTable
    {
        private Elem[] Table { get; }
        public int Size { get; }
        public LineHashTable(int size = 100) // Конструктор
        {
            Size = size;
            Table = new Elem[size];
        }
        public int GetKey(string name)
            // получение ключа
        {
            int k1 = name.Length;
            int k2 = (int)name[0];
            int k3 = (int)name[name.Length - 1];
            return (k1 * k2 + k3);
        }

        public bool Add(Film film)
        {
            if (Find(film.Name) != null)
            {
                return false;
            }
            int hash = GetKey(Film.Hash(film.Name));
            int index = hash % Size;
            if (Table[index] == null || Table[index].Deleted) // Добавление в таблицу
            {
                Table[index] = new Elem()
                {
                    Film = film
                };
                return true;
            }
            for (int i = index + 1; i < Size; i++) // Метод решения коллизии
            {
                if (Table[i] == null || Table[i].Deleted)
                {
                    Table[i] = new Elem()
                    {
                        Film = film
                    };
                    return true;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null || Table[i].Deleted)
                {
                    Table[i] = new Elem()
                    {
                        Film = film
                    };
                    return true;
                }
            }
            return false;
        }
        public Film Find(string name)
        {
            int hash = GetKey(Film.Hash(name));
            int index = hash % Size;
            if (Table[index] == null)
            {
                return null;
            }
            if (!Table[index].Deleted && Table[index].Film.Name == name)
            {
                return Table[index].Film;
            }
            for (int i = index + 1; i < Size; i++)
            {
                if (Table[i] == null)
                {
                    return null;
                }
                if (!Table[i].Deleted && Table[i].Film.Name == name)
                {
                    return Table[i].Film;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null)
                {
                    return null;
                }
                if (!Table[i].Deleted && Table[i].Film.Name == name)
                {
                    return Table[i].Film;
                }
            }
            return null;
        }
        public void Delete(string name)
        {
            int hash = GetKey(Film.Hash(name));
            int index = hash % Size;
            if (Table[index] == null)
            {
                return;
            }
            if (!Table[index].Deleted && Table[index].Film.Name == name)
            {
                Table[index].Deleted = true;
                return;
            }
            for (int i = index + 1; i < Size; i++)
            {
                if (Table[i] == null)
                {
                    return;
                }
                if (!Table[i].Deleted && Table[i].Film.Name == name)
                {
                    Table[i].Deleted = true;
                    return;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null)
                {
                    return;
                }
                if (!Table[i].Deleted && Table[i].Film.Name == name)
                {
                    Table[i].Deleted = true;
                    return;
                }
            }
        }
        public void Clear()
        {
            for (int i = 0; i < Size; i++)
            {
                Table[i] = null;
            }
        }

        public List<Film> GetData()
        {
            List<Film> list = new List<Film>();
            foreach (Elem e in Table)
            {
                if (e != null && !e.Deleted)
                {
                    list.Add(e.Film);
                }
            }
            return list;
        }
    }
    class Elem
    {
        public Film Film { get; set; }
        public bool Deleted { get; set; } = false;

    }
}
